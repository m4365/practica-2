#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../include/mdp.h"
#include "../include/io.h"

#define MUTATION_RATE 0.15
#define PRINT 0

int aleatorio(int n) {
	return (rand() % n);  // genera un numero aleatorio entre 0 y n-1
}

float random_float(float min, float max)
{
	if (min == max) {
		return min;
	} else if(min < max) {
		return (max - min) * ((float)rand() / RAND_MAX) + min;
	}

	return 0;
}

int find_element(int *array, int end, int element)
{
	int i=0;
	int found=0;
	
	// comprueba que un elemento no está incluido en el individuo (el cual no admite enteros repetidos)
	while((i < end) && ! found) {
		if(array[i] == element) {
			found = 1;
		}
		i++;
	}
	return found;
}

int *crear_individuo(int n, int m)
{
	int i=0, value;
	int *individuo = (int *) malloc(m * sizeof(int));
	
	// inicializa array de elementos
	memset(individuo, -1, m * sizeof(int));
	
	while(i < m) {
		value = aleatorio(n);
		// si el nuevo elemento no está en el array...
		if(!find_element(individuo, i, value)) {
			individuo[i] = value;  // lo incluimos
			i++;
		}
	}
	return individuo;
}

int comp_array_int(const void *a, const void *b) {
	return (*(int *)a - *(int *)b);
}

int comp_fitness(const void *a, const void *b) {
	/* qsort pasa un puntero al elemento que está ordenando */
	return (*(Individuo **)a)->fitness - (*(Individuo **)b)->fitness;
}

/**
 * @brief 
 * 
 * @param d 
 * @param n tamano del conjunto A
 * @param m tamano de cada subconjunto B
 * @param n_gen numero de generaciones
 * @param tam_pob tamano de la poblacion de individuos.
 * @param sol 
 * @return double 
 */
double aplicar_mh(const double *d, int n, int m, int n_gen, int tam_pob, int *sol)
{
	int i, g, mutation_start;
	
	// crea poblacion inicial (array de individuos)
	Individuo **poblacion = (Individuo **) malloc(tam_pob * sizeof(Individuo *));
	assert(poblacion);
	
	// crea cada individuo (array de enteros aleatorios)
	for(i = 0; i < tam_pob; i++) {
		poblacion[i] = (Individuo *) malloc(sizeof(Individuo));
		poblacion[i]->array_int = crear_individuo(n, m);
		
		// calcula el fitness del individuo
		fitness(d, poblacion[i], n, m);
	}
	
	// ordena individuos segun la funcion de bondad (mayor "fitness" --> mas aptos)
	qsort(poblacion, tam_pob, sizeof(Individuo *), comp_fitness);
	
	// evoluciona la poblacion durante un numero de generaciones
	for(g = 0; g < n_gen; g++)
	{
		// los hijos de los ascendientes mas aptos sustituyen a la ultima mitad de los individuos menos aptos
		for(i = 0; i < (tam_pob/2) - 1; i += 2) {
			cruzar(poblacion[i], poblacion[i+1], poblacion[tam_pob/2 + i], poblacion[tam_pob/2 + i + 1], n, m);
		}
		
		// inicia la mutacion a partir de 1/4 de la poblacion
		mutation_start = tam_pob/4;
		
		// muta 3/4 partes de la poblacion
		for(i = mutation_start; i < tam_pob; i++) {
			mutar(poblacion[i], n, m);
		}
		
		// recalcula el fitness del individuo
		for(i = 0; i < tam_pob; i++) {
			fitness(d, poblacion[i], n, m);
		}
		
		// ordena individuos segun la funcion de bondad (mayor "fitness" --> mas aptos)
		qsort(poblacion, tam_pob, sizeof(Individuo *), comp_fitness);
		
		if (PRINT) {
			printf("Generacion %d - ", g);
			printf("Fitness = %.0lf\n", -(poblacion[0]->fitness));
		}
	}
	
	// ordena el array solucion
	qsort(poblacion[0]->array_int, m, sizeof(int), comp_array_int);
	memmove(sol, poblacion[0]->array_int, m*sizeof(int));
	
	// almacena el mejor valor obtenido para el fitness
	double value = -(poblacion[0]->fitness);
	
	// se libera la memoria reservada
	free(poblacion);
	
	// devuelve el valor obtenido para el fitness
	return value;
}

void cruzar(Individuo *padre1, Individuo *padre2, Individuo *hijo1, Individuo *hijo2, int n, int m)
{
	// Elegir un "punto" de corte aleatorio a partir del que se realiza el intercambio de los genes
	int pointCut = aleatorio(m);
	// Los primeros genes del padre1 van al hijo1. Idem para el padre2 e hijo2.
	for(int i = 0; i < pointCut; i++) {
		hijo1->array_int[i] = padre1->array_int[i];
		hijo2->array_int[i] = padre2->array_int[i];
	}
	
	// Y los restantes son del otro padre, respectivamente.
	for(int i = pointCut; i < m; i++) {
		// Factibilizar: eliminar posibles repetidos de ambos hijos
		// Si encuentro alguno repetido en el hijo1, lo cambio por otro que no este en el conjunto
		int parentElement = padre2->array_int[i];
		while(find_element(hijo1->array_int, m, parentElement)) {
			parentElement = aleatorio(n);
		}
		hijo1->array_int[i] = parentElement;
	}

	// Y los restantes son del otro padre, respectivamente.
	for(int i = pointCut; i < m; i++) {
		// Factibilizar: eliminar posibles repetidos de ambos hijos
		// Si encuentro alguno repetido en el hijo1, lo cambio por otro que no este en el conjunto
		int parentElement = padre1->array_int[i];
		while(find_element(hijo2->array_int, m, parentElement)) {
			parentElement = aleatorio(n);
		}
		hijo2->array_int[i] = parentElement;
	}
}

double distancia_ij(const double *d, int i, int j, int n)
{
	// I commented this line the distance from i to j should be the value of the set instead of 0
	//double dist = 0.0;
	
	// Devuelve la distancia entre dos elementos de la matriz 'd'
	return d[(i * n) + j];
}

// Determina la calidad del individuo calculando la suma de la distancia entre cada par de enteros
void fitness(const double *d, Individuo *individuo, int n, int m)
{
	individuo->fitness = 0;
	for(int i = 1; i < m; i++) {
		individuo->fitness += distancia_ij(d, individuo->array_int[i - 1], individuo->array_int[i], n);	
	}
}
	
/**
 * @brief 
 * 
 * @param actual 
 * @param n size of set A 
 * @param m 
 */
void mutar(Individuo *actual, int n, int m)
{
	// Decidir cuantos elementos mutar:
	// Si el valor es demasiado pequeño la convergencia es muy pequeña y si es demasiado alto diverge
	float mutationRate = MUTATION_RATE;
	
	for (int i = 0; i < m; i++)
	{
		if (mutationRate < random_float(0,1)) {
			continue;
		}
		// Cambia el valor de algunos elementos de array_int de forma aleatoria
		int newElement = aleatorio(n);
		// teniendo en cuenta que no puede haber elementos repetidos.
		while(find_element(actual->array_int, m, newElement)) {
			// Cambia el valor de algunos elementos de array_int de forma aleatoria
			newElement = aleatorio(n);
		}

		actual->array_int[i] = newElement;
	}
}
